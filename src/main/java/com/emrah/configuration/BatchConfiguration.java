package com.emrah.configuration;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class BatchConfiguration {

	private final JobBuilderFactory jobBuilderFactory;
	private final StepBuilderFactory stepBuilderFactory;
	
	@Bean
	public Tasklet loadStockFileTasklet() {
		return (contribution, chunkContext) -> {
			System.out.println("The stock file has been loaded.");
			return RepeatStatus.FINISHED;
		};
	}
	
	@Bean
	public Tasklet loadCustomerFileTasklet() {
		return (contribution, chunkContext) -> {
			System.out.println("The customer file has been loaded.");
			return RepeatStatus.FINISHED;
		};
	}
	
	@Bean
	public Tasklet updateStartTasklet() {
		return (contribution, chunkContext) -> {
			System.out.println("The start has been updated.");
			return RepeatStatus.FINISHED;
		};
	}
	
	@Bean
	public Tasklet runBatchTasklet() {
		return (contribution, chunkContext) -> {
			System.out.println("The batch has been run.");
			return RepeatStatus.FINISHED;
		};
	}
	
	@Bean
	public Step loadFileStep() {
		return stepBuilderFactory.get("loadFileStep")
				.tasklet(loadStockFileTasklet())
				.build();
	}
	
	@Bean
	public Step loadCustomerStep() {
		return stepBuilderFactory.get("loadCustomerStep")
				.tasklet(loadCustomerFileTasklet())
				.build();
	}
	
	@Bean
	public Step updateStartStep() {
		return stepBuilderFactory.get("updateStartStep")
				.tasklet(updateStartTasklet())
				.build();
	}
	
	@Bean
	public Step runBatchStep() {
		return stepBuilderFactory.get("runBatchStep")
				.tasklet(runBatchTasklet())
				.build();
	}
	
	@Bean
	public Flow preProcessingFlow() {
		return new FlowBuilder<Flow>("preProcessingFlow")
				.start(loadFileStep())
				.next(loadCustomerStep())
				.next(updateStartStep())
				.build();
	}
	
	@Bean
	public Step initializeBatchStep() {
		return stepBuilderFactory.get("initializeBatchStep")
				.flow(preProcessingFlow())
				.build();
	}
	
	@Bean
	public Job job() {
		return jobBuilderFactory.get("job26")
				.start(initializeBatchStep())
				.next(runBatchStep())
				.build();
	}
	
}
